package gabor.razvan.Lab3.ex3;

public class Author {
    String name;
    String email;
    char gender;

    public Author(String name, String email, char gender) {
        this.name = name;
        this.email = email;
        this.gender = gender;
    }

    public void setEmail() {
        this.email = this.email + "@yahoo.com";
    }

    public String getEmail() {
        System.out.println("The email adress is " + this.email);
        return this.email;
    }

    public String getName() {
        System.out.println("The name is " + this.name);
        return this.name;
    }

    public char getGender() {
        System.out.println("The gender is " + this.gender);
        return (this.gender);
    }

    public String toString() {
        return ("" + this.name + "(" + gender + ") at " + this.email);

    }
}
