package gabor.razvan.Lab3.ex2;

public class TestCircle {
    public static void main (String[] args) {
        Circle c1 = new Circle(5);
        double circumference1 = c1.getCircumference(); 
        double area1 = c1.getArea();
        System.out.println("Radius of c1: " + c1.getRadius);
        System.out.printf("The circle c1 with circumference %.3f has area %.3f%n", circumference1, area1);

        Circle c2 = new Circle(10); 
        double area2 = c2.getArea(); //objectReference.methodName()
        double circumference2 = c2.getCircumference(); //objectReference.methodName()
        System.out.println("Radius of c2: " + c2.getRadius);
        System.out.printf("The circle c2 with circumference %.3f has area %.3f%n", circumference2, area2);
    }
}