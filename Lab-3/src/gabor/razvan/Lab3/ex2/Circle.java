package gabor.razvan.Lab3.ex2;

public class Circle {
    private double radius;

    public Circle(double radius) {
        this.radius = radius; // this.radius refers to the instance variable above
    }

    public Circle() {
    }

    public double getArea() {
        double area = radius * radius * Math.PI;
        return area;
    }

    public double getCircumference() {
        double circumference = 2 * Math.PI * radius; 
        return circumference;
    }
}