package gabor.razvan.Lab3.ex4;

import java.io.IOException;

public class MyPoint {
    int x;
    int y;

    public MyPoint() {
        this.x = 0;
        this.y = 0;
    }

    public MyPoint(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void setXY(int x, int y) throws IOException {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return this.x;
    }

    public int getY() {
        return this.y;
    }

    public String getPoint() {
        System.out.println("The x coordinate is " + getX() + "and the y is " + getY());
        return "" + this.x + this.y;
    }

    @Override
    public String toString() {
        return "(" + this.x + "," + this.y + ")";
    }

    public double distance(int x, int y) {
        double result;
        result = Math.sqrt(Math.pow(this.x - x, 2) + Math.pow(this.y - y, 2));
        return result;
    }

    public double distance(MyPoint anotherPoint) {
        double dist = Math.sqrt(Math.pow(this.getX() - anotherPoint.getX(), 2) + Math.pow(this.y - anotherPoint.getY(), 2));
        return dist;
    }
}
