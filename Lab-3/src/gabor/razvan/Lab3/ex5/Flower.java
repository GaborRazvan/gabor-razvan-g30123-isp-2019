package gabor.razvan.Lab3.ex5;

public class Flower {
    int petal;
    static int number=0;
    Flower(int p){
        number++;
        petal=p;
        System.out.println("New flower has been created!");
    }
    public void getNumber(){
        System.out.println("were created "+number+" objects");
    }

    public static void main(String[] args) {
        Flower f1 = new Flower(4);
        Flower f2 = new Flower(6);
        f1.getNumber();
    }
}
