package gabor.razvan.Lab4.ex3;
import gabor.razvan.Lab4.ex2.Author;

public class TestBook {
    public static void main(String[] args) {
        Author A = new Author("Gabor", "gabor.razvan@yahoo.com", 'm');
        Book B1 = new Book("Gabor","gabor.razvan@yahoo.com",'m',"Amintiri din copilarie ",A,250);
        System.out.println("The author of B1 is "+ B1.getAuthor());
        System.out.println("The price of the book 'The Book' is "+ B1.getPrice());
        B1.setPrice(200);
        System.out.println("The new price of the book 'The Intelligent Investor ' is "+ B1.getPrice());
        System.out.println("The quantity of the book 'Think & Grow Rich' is "+ B2.getQtyInStock());
    }
}