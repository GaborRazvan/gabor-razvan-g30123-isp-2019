package gabor.razvan.Lab4.ex4;

import java.util.Arrays;
import gabor.razvan.Lab4.ex2.Author;
public class TestBooks {
    public static void main(String[] args) {
        Author[] aut = new Author[3];
        aut[0] = new Author("Elton John", "eltonjohn@gmail.com", 'm');
        aut[1] = new Author("William Shirer", "williamshirer@yahoo.com", 'm');
        aut[2] = new Author("Dee Brown", "deebrown@yahoo.com", 'm');
        Book b = new Book("Yes", aut, 75, 12);
        Book b1 = new Book("The Rise and Fall of the Third Reich", aut, 45, 20);
        Book b2 = new Book("The American West", aut, 27, 32);
        System.out.println(b.toString() + "\n");
        b.setPrice(20.5);
        b.setQtyInStock(40);
        b.printAuthors();
        b1.setPrice(30);
        b1.setQtyInStock(15);
        b1.printAuthors();
        b2.setPrice(50.1);
        b2.setQtyInStock(20);
        b2.printAuthors();
        System.out.println(Arrays.toString(b.getAuthor()) + " " + b.getName() + " " + b.getPrice() + " " + b.getQtyInStock());
        System.out.println(Arrays.toString(b1.getAuthor()) + " " + b1.getName() + " " + b1.getPrice() + " " + b1.getQtyInStock());
        System.out.println(Arrays.toString(b2.getAuthor()) + " " + b2.getName() + " " + b2.getPrice() + " " + b2.getQtyInStock());
    }
}