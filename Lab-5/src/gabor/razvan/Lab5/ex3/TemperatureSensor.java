package gabor.razvan.Lab5.ex3;

import java.util.Random;

public class TemperatureSensor extends Sensor {
    Random r = new Random();

    public int readValue() {
        return r.nextInt(101);
    }
}