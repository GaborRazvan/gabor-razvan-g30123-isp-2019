package gabor.razvan.Lab5.ex1;


public class Circle extends Shape {
    protected double radius;

    public Circle() {
    }

    public Circle(double radius) {
        this.radius = radius;
    }

    public Circle(double radius, String color, boolean filled) {
        super(color, filled);
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public double getArea() {
        return 3.14 * this.radius * this.radius;
    }

    @Override
    public double getPerimeter() {
        return 3.14 * 2 * this.radius;
    }

    @Override
    public String toString() {
        return "Circle with area: " + getArea() + " and perimeter: " + getPerimeter() + ", color:" + super.getColor() + "is filled: " + super.isFilled();
    }
}