package gabor.razvan.Lab5.ex1;

public class Square extends Rectangle{
    protected double side;

    public Square() {
    }

    public Square(double side) {
        this.side = side;
    }

    public Square(double side, String color, boolean filled){
        super(side,side,color,filled);

    }

    public double getSide() {
        return side;
    }

    public void setSide(double side) {
        this.side = side;
    }

    public void setWidth(double side){
        super.width=side;
    }

    public void setLength(double side){
        super.length=side;
    }
    public String toString(){
        return "Square with area: "+super.getArea()+"and Perimeter: "+super.getPerimeter()+", filled "+super.isFilled()+"and color "+super.getColor();
    }

}
