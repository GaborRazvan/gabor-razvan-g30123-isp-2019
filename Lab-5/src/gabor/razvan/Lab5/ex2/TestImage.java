package gabor.razvan.Lab5.ex2;

public class TestImage {
    public static void main(String[] args) {
        RealImage r = new RealImage("Fisier");
        r.display();
        ProxyImage p1 = new ProxyImage("PFisier");
        p1.display();
        Image i1 = new RealImage("test");
        i1.display();

        ProxyImage p2 = new ProxyImage("test2");
        p2.display();
        p2.RotatedImage();
        p2 = new ProxyImage("test2", 1);
        p2.display();

    }
}