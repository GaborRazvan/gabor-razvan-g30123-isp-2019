package gabor.razvan.Lab7.ex2;

import java.io.*;
import java.util.*;

public class CharacterCounter {
    public static void main (String args[]) throws IOException{

        char character;
        BufferedReader stdin = new BufferedReader(
                new InputStreamReader(System.in));
        System.out.println("Give the character:");
        character=stdin.readLine().charAt(0);

        int number=0;

        BufferedReader in = new BufferedReader(
                new FileReader("E:\\Programe\\eclipse2\\gabor-razvan-g30123-isp-2019\\Lab-7\\src\\gabor\\razvan\\Lab7\\ex2\\data.txt"));
        String s;
        while((s = in.readLine())!= null)
        {
            char[] lin = s.toCharArray();
            for (char c : lin) {
                if (c == character) {
                    number++;
                }
            }
        }
        in.close();
        System.out.println("The character " +character+  " appears " +number+ " times");
    }

}