package gabor.razvan.Lab7.ex1;

class CofeeMaker {
    private static int coffeeCount = 0;
    private int LIMIT = 5;

    Coffee makeCofee() throws TooManyObjectsException {
        if (coffeeCount >= LIMIT) {
            throw (new TooManyObjectsException("To much coffee made"));
        }
        System.out.println("Make a coffe");
        coffeeCount++;
        int t = (int) (Math.random() * 100);
        int c = (int) (Math.random() * 100);
        return new Coffee(t, c);
    }
}
